#!/usr/bin/env bash

#created by: Ilay ron
#purpose chepter 27 exercises
#date 24/10/2020
#version: v1.1.2



Main(){

Logged_in_user
Deco
List_of_all_loggedin_users
Deco
List_of_all_users_with_last_command
Deco
Unique_user_id
Deco
Select_the_option_you_want
}

Logged_in_user(){

login_user=$(who am i)

echo "you lou currntly logged in with $login_user"

}


List_of_all_loggedin_users(){

list_of_login_users=$(who)

echo "here is a list of all logged on users $list_of_login_users"

}

List_of_all_users_with_last_command(){

_list_of_all_users_with_last_command=$(w)

echo "display all users with their last command $_list_of_all_users_with_last_command"

}

Unique_user_id(){

unique_user_id=$(id)

echo "your user name and your unique user identification is $unique_user_id"

}

Select_the_option_you_want(){

options=("Switch_user" "Switch_anther_user" "Try_to_create_new_user" "create_new_user_with_sudo")

echo "choose the option you want: "

select opt in ${options[@]}
do
case $opt in
"switch_user")

su pirate
;;
"switch_anther_user")
su - student
;;
"Try_to_create_new_user")

useradd ilay

;;
"create_new_user_with sudo")
sudo /usr/sbin/useradd ilay
;;
"quit")
break
;;
esac
done
}


Deco(){

      _time=3.5
      l="#######################################"
      printf "\n$l\n # %s\n$l" "$@"
      sleep $_time
      clear

}


Main
