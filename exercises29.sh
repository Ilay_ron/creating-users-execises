#!/usr/bin/env bash

#created by: Ilay Ron
#purpose: exercises chepter 29
#date: 24/10/2020
#version: v1.0.7


main(){

Change_serena_passwd
Deco
Change_venus_passwd
Deco
Unlock_serena
Deco
Difference_passwdd_and_passwdL
Deco
Change_passwd_10_days
Deco
Change_passwd_for_new_users
Deco
List_all_shells

}


Change_serena_passwd(){

passwd Serena

}

Change_venus_passwd(){

passwd Venus

}

unlock_serena(){
passwd -d Serena
}

Difference_passwdd_and_passwdL(){

echo "Locking will prevent the user from logging on to the system with his password by putting
a ! in front of the password in /etc/shadow.
Disabling with passwd will erase the password from /etc/shadow."
}

Change_passwd_10_days(){

chage -m 10 Serena

}

Change_passwd_for_new_users(){

vi /etc/login.defs < PASS_MAX_DAYS=10


}

List_all_shells(){

chsh -l
cat /etc/shells


}

Deco(){

      _time=3.5
      l="#######################################"
      printf "\n$l\n # %s\n$l" "$@"
      sleep $_time
      clear

}
main
