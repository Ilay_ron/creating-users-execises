#!/usr/bin/env bash

#created by: Ilay Ron
#Purpose: exercises chepter 28
#date: 24/10/2020
#version: v1.0.4


main(){

#create a user name Serena

add_user
deco
add_user_with_bash
deco
verify_the_user_entries
deco
Verify_their_home_dir_created
deco
create_user_name_with_bindate
deco
creating_test_user
deco
change_defaul_serena
}
     
add_user(){


useradd -m -c "Serena Williams" Serena
echo " the users Serena created successfully"

} 

add_user_with_bash(){

useradd -m-c "Venus Williams" -s /bin/bash "Venus"     

echo " user created successfully with bin/bash"
}

verify_the_user_entries(){

tail -2 /etc/passwd

}

Verify_their_home_dir_created(){


ls -lrt /home |tail -2

}


create_user_name_with_bindate(){

useradd -s /bin/date eistime
}

create_file_in_skell(){

echo welcome > /etc/skel/welcome.txt

}
creating_test_user(){
useradd -m "test user"

ls -l

userdel -r test user

}

change_defaul_serena(){
usermod -s /bin/bash Serena
}

deco(){
      time=2.5
      l="#######################################"
      printf "\n$l\n # %s\n$l" "$@"
      sleep $_time
      clear
}

main
